Welcome to the **Documentation Wiki** for [Complaints Hub](https://gitlab.com/MadeByThePinsTeam-DevLabs/legal/complaints-hub) GitLab project. To continue, use the navigation bar or the [**Navigating Around The Wiki**](#navigating-around-the-wiki) section. This wiki is work in progress and still under construction, but we're open for user contributions, but we recommend to see

## Navigating Around The Wiki

* [Contributing and Code of Conduct](Contributing Policies)
* [The Issue Templates](Issue Templates)
  * [Security Vunlerabilities](Issue Templates/Vulnerabilities)
  * [Non-compliant to Code of Conduct](Issue Templates/Non-compliance of Code of Conduct)
* [How To Report](How To Report)
  * [Security Vunlerabilities](How To Report/Vunlerabilities)
  * [Non-compliance to Code of Conduct](How To Report)
  * [IP Infringement](How To Report/IP Infringement)
* [Frequently Asked Questions](FAQs)
* [Addressing Grievances](Grievances 101)

## How To Contribute
Our wiki is open to contributions from the community, especially from the attorneys and lawyers.

## How To Purpose Edits/Improvements
[Submit your suggestions to the Wiki Repository mirror](https://gitlab.com/MadeByThePinsTeam-DevLabs/wiki-repo-clones/complaints-hub) and when approved and merged, it will push to here every hour.