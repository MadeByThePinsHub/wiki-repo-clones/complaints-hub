# Contributing Guide and Code of Conduct
When contributing to this wiki, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change.

Please note we have a [code of conduct](#code-of-conduct), please follow it in all your interactions with the project.

## Wiki Editing Guide
1. Edit with caution, as others may be editing the wiki at the same time. [Use the wiki drafting repository instead.](https://gitlab.com/MadeByThePinsTeam-DevLabs/wiki-repo-clones/complaints-hub).
